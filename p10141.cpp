#include<cstdio>
#include<iostream>

using namespace std;

int n, p, r;
float d, bc, bcst;
string line, name, selected_name;

int main() {
    for(int T = 1;; T++) {
        scanf("%d %d\n", &n, &p);
        if(n == 0 && p == 0) break;
        if(T > 1) cout << endl;
        for(int i = 0; i < n; i++)
            getline(cin, line);
        bc = 0.0;
        bcst = 1E37;
        for(int i = 0; i < p; i++) {
            getline(cin, name);
            scanf("%f %d\n", &d, &r);
            for(int i = 0; i < r; i++)
                getline(cin, line);
            if((float) r / p > bc \
                    || (float) r / p == bc && d < bcst) {
                bc = (float) r / p;
                bcst = d;
                selected_name = name;
            }
        }
        printf("RFP #%d\n", T);
        cout << selected_name << endl;
    }
}
